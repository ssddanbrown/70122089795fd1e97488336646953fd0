*Originally put together when submitting to laravelexamples.com*

I'm not particularly proud of the code-base in terms of cleanliness and style, but I'm proud of where it's got to over the last 6 years. Over those years my experience, knowledge and opinions have changed; And the code-base can often reflect that as inconsistency.

- Custom Guard (Also LDAP support)
  - Guard Class: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Auth/Access/Guards/LdapSessionGuard.php
  - Registration in provider: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Providers/AuthServiceProvider.php

- External App Extension Support
  - Allows a kind of wordpress-style extension of app logic, via a `functions.php` file in an out-of-app user theme folder.
  - Documentation: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/dev/docs/logical-theme-system.md
  - Service Provider: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Providers/ThemeServiceProvider.php
  - Service: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Theming/ThemeService.php
  - Events: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Theming/ThemeEvents.php

- Custom 'config' Directory
  - This has proved quite useful in providing an open-source project, to prevent users editing core config files. Can cause issues with Laravel shift though.
  - Extension of Laravel Application class: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Application.php
  - Usage in application boostrapping: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/bootstrap/app.php#L14-L16

- Custom Debug View Display (Instead of flare)
  - Registration in AppServiceProvider: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Providers/AppServiceProvider.php#L76-L78
  - Whoops handler implementation: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Exceptions/WhoopsBookStackPrettyHandler.php
  - View file: https://github.com/BookStackApp/BookStack/blob/master/resources/views/errors/debug.blade.php

- Usage of CSP via Middleware and Nonce-based CSP for JavaScript
  - Middleware: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Http/Middleware/ApplyCspRules.php
  - Service: https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Util/CspService.php
  - Usage of nonce (Shared in middleware): https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/resources/views/layouts/base.blade.php#L53

- Support of instance view override support
  - I also do this for language & icon files to allow my users to override.
  - Documentation: https://github.com/BookStackApp/BookStack/blob/master/dev/docs/visual-theme-system.md
  - Within config (Not very neat): https://github.com/BookStackApp/BookStack/blob/05ef23d34e6346a1e4c05bc38eb7e5777180c514/app/Config/view.php